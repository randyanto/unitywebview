﻿/*
 * Copyright (C) 2012 GREE, Inc.
 * Copyright (C) 2016 Yonathan Randyanto
 * 
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 * 
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 * 
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */


using UnityEngine;

public class RWebViewObject : MonoBehaviour {
				
	bool visibility;		
	AndroidJavaObject webView;				
		
#region EVENTS

	void OnDestroy(){			
		if (webView == null)
			return;
		webView.Call("Destroy");
		webView = null;			
	}

#endregion

#region PUBLIC

	public void Init()	{			
		webView = new AndroidJavaObject("com.randyanto.randywebview.RWebViewPlugin");
		webView.Call("Init");			
	}						
	
	public void SetMargins(int left, int top, int right, int bottom){			
		if (WebViewIsAvailable()){
			webView.Call("SetMargins", left, top, right, bottom);			
		}
	}

	public void LoadURL(string url){
		if (string.IsNullOrEmpty(url))
			return;			
		if (WebViewIsAvailable()){
			webView.Call("LoadURL", url);			
		}
	}
	
	public void EvaluateJS(string js){	
		if (WebViewIsAvailable()){
			webView.Call("LoadURL", "javascript:" + js);			
		}
	}

#endregion PUBLIC

#region PRIVATE

	private bool WebViewIsAvailable(){
		return (webView != null);
	}

#endregion

}
