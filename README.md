# unity-webview

Simplified GREE unity-webview just for Android.
Unity-webview is a plugin for Unity 5 that overlays WebView components
on Unity view. It works on Android.

This work is derived from https://github.com/gree/unity-webview
The plugin source code can be found at https://bitbucket.org/randyanto/uniandrowebview

